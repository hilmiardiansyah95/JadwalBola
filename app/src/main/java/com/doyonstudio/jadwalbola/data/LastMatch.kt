package com.doyonstudio.jadwalbola.data

import com.google.gson.annotations.SerializedName

data class LastMatch(
        @SerializedName("strHomeTeam")
        var homeTeam: String? = null,

        @SerializedName("intHomeScore")
        var homeScore: String? = null,

        @SerializedName("strAwayTeam")
        var awayTeam: String? = null,

        @SerializedName("intAwayScore")
        var awayScore: String? = null,

        @SerializedName("dateEvent")
        var dateMatch: String? = null
)