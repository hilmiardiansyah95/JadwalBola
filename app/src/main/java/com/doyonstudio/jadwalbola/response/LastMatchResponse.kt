package com.doyonstudio.jadwalbola.response

import com.doyonstudio.jadwalbola.data.LastMatch
import com.google.gson.annotations.SerializedName

data class LastMatchResponse(
        @SerializedName("events")
        val lastMatch: List<LastMatch>
)