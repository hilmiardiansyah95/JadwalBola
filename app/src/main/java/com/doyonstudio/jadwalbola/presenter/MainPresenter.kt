package com.doyonstudio.jadwalbola.presenter

import com.doyonstudio.jadwalbola.view.MainView
import com.doyonstudio.jadwalbola.api.ApiClient
import com.doyonstudio.jadwalbola.api.ApiRepo
import com.doyonstudio.jadwalbola.response.LastMatchResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter(private val view: MainView) {

    fun getLastMatch(){
        view.showLoading()

        doAsync {
            val data = Gson().fromJson(
                    ApiRepo().doRequest(ApiClient.getLastMatch()),
                    LastMatchResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showLastMatch(data.lastMatch)
            }
        }
    }
}