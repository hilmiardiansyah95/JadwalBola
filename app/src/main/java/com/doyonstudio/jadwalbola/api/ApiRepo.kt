package com.doyonstudio.jadwalbola.api

import java.net.URL

class ApiRepo {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}