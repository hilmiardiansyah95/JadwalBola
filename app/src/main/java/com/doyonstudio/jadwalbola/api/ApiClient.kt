package com.doyonstudio.jadwalbola.api

import com.doyonstudio.jadwalbola.BuildConfig

object ApiClient {
    fun getLastMatch(): String {
         return "${BuildConfig.BASE_URL}/api/v1/json/${BuildConfig.TSDB_API_KEY}/eventspastleague.php?id=4328"
    }
}